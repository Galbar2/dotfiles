fun! promptline#presets#alessio#get()
  return {
        \'a': [ promptline#slices#host(), promptline#slices#user() ],
        \'b': [ promptline#slices#cwd() ],
        \'c': [ '$(if [[ "$VIRTUAL_ENV" != "" ]]; then echo "ⓔ"; fi)', promptline#slices#jobs() ],
        \'warn' : [ promptline#slices#last_exit_code() ],
        \'x' : [ promptline#slices#git_status() ],
        \'z' : [ promptline#slices#vcs_branch() ]}
endfun
