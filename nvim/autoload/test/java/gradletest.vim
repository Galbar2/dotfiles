if !exists('g:test#java#gradletest#file_pattern')
  let g:test#java#gradletest#file_pattern = '\v^.*[Tt]est\.java$'
endif

function! test#java#gradletest#test_file(file) abort
  return a:file =~? g:test#java#gradletest#file_pattern
endfunction

function! test#java#gradletest#build_position(type, position) abort
  let filename = fnamemodify(a:position['file'], ':.:r')
  let classname = substitute(substitute(filename, '^.\{-}java\/', '', ''), '/', '.', 'g')

  let test_mode = 'test'
  if filename =~? '\v^.*integration.*'
    let test_mode = 'inttest'
  endif

  if a:type == 'nearest'
    let name = s:nearest_test(a:position)
    if !empty(name)
      return [test_mode, '--tests', classname.'.'.name]
    else
      return [test_mode, '--tests', classname]
    endif
  elseif a:type == 'file'
    return [test_mode, '--tests', classname]
  else
    return [test_mode]
  endif
endfunction

function! test#java#gradletest#build_args(args) abort
  return a:args
endfunction

function! test#java#gradletest#executable() abort
  return 'gradle'
endfunction

function! s:nearest_test(position) abort
  let name = test#base#nearest_test(a:position, g:test#java#patterns)
  return join(name['test'], '.')
endfunction
