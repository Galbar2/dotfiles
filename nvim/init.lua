--
-- Custom Neovim init.lua file
--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

-- Stand-alone modules
require('settings')
require('providers')
require('keymappings')
require('clean-spaces-on-save')
require('custom-inputs').setup()
require('ctrl-p').setup()

-- Load plugins
require('plugins')

-- Modules depending on plugins
require('treesitter')
require('theme')
require('lang')

function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

