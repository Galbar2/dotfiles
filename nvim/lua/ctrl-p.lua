--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--


local M = {}

M.setup = function()
    local options = '--preview "bat -n --color=always {} --theme=srcery"'
    vim.api.nvim_set_keymap(
        'n',
        '<C-p>',
        "<cmd>call fzf#run(fzf#wrap({'source': 'ag --files-with-matches', 'options': '" .. options .. "'}))<CR>",
        { noremap=true, silent=true }
    )
end

return M

