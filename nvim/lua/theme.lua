--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--


vim.o.termguicolors = true
vim.cmd "hi Normal guibg=NONE ctermbg=NONE"
vim.cmd.colorscheme('srcery')
-- vim.cmd "autocmd VimEnter * colorscheme srcery"

-- vim-airline
vim.g['airline#extensions#tabline#enabled'] = 1
vim.g['airline_powerline_fonts'] = 1
vim.g['airline_theme'] = 'murmur'
