--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

-- Avoid typos
vim.api.nvim_set_keymap('', ':W', ':w', {noremap = true})
vim.api.nvim_set_keymap('', ':Q', ':q', {noremap = true})
vim.api.nvim_set_keymap('', ':E', ':e', {noremap = true})

-- Yank 'till the end of the line
vim.api.nvim_set_keymap('n', 'Y', 'y$', {noremap = true})

-- Visual shifting (does not exit Visual mode)
vim.api.nvim_set_keymap('v', '<', '<gv', {noremap = true})
vim.api.nvim_set_keymap('v', '>', '>gv', {noremap = true})

-- Reset the search
vim.api.nvim_set_keymap('n', '<leader>/', ':nohlsearch<CR>', {silent = true})

-- Spellcheck toggle (on/off)
vim.api.nvim_set_keymap('n', '<leader>s', ':set spell!<CR>', {silent = true})

-- Using tabs
vim.api.nvim_set_keymap('n', 'tt', ':tabedit<Space>', {noremap = true})
vim.api.nvim_set_keymap('n', 'tm', ':tabm<Space>', {noremap = true})
vim.api.nvim_set_keymap('n', 'tc', ':tabclose<CR>', {noremap = true})
vim.api.nvim_set_keymap('n', '<S-h>', 'gT', {noremap = true})
vim.api.nvim_set_keymap('n', '<S-l>', 'gt', {noremap = true})
