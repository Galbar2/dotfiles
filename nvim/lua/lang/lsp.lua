--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

local function err_message(...)
  vim.notify(table.concat(vim.tbl_flatten{...}), vim.log.levels.ERROR)
  vim.api.nvim_command("redraw")
end

local menu_opts = {}
local menu_keys = {}
local function menu_callback(opt)
    menu_opts[opt]()
end

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local function on_attach(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  -- Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local mapping_opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', mapping_opts)
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', mapping_opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', mapping_opts)
  buf_set_keymap('i', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', mapping_opts)
  buf_set_keymap('n', '<A-Enter>', '<cmd>lua vim.lsp.buf.code_action()<CR>', mapping_opts)
  buf_set_keymap('v', '<A-Enter>', '<cmd>lua vim.lsp.buf.code_action()<CR>', mapping_opts)
  buf_set_keymap('n', '<Leader>c', '<cmd>lua vim.lsp.buf.code_action()<CR>', mapping_opts)
  buf_set_keymap('v', '<Leader>c', '<cmd>lua vim.lsp.buf.code_action()<CR>', mapping_opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', mapping_opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', mapping_opts)
  buf_set_keymap('i', '<C-l>', '<cmd>lua require("lang/lsp").complete_done()<CR><ESC>', mapping_opts)
end

local nvim_lsp = require('lspconfig')
local ctx_menu = require('contextual-menu')
local custom_inputs = require('custom-inputs')

local M = {}

M.show_menu = function()
    vim.ui.select(menu_keys, {}, menu_callback)
end

M.complete_done = function()
    local completed_item = vim.api.nvim_get_vvar('completed_item')
    if not completed_item or not completed_item.user_data then
        return
    end
    local lnum, col = unpack(vim.api.nvim_win_get_cursor(0))
    local item = completed_item.user_data.nvim.lsp.completion_item
    local bufnr = vim.api.nvim_get_current_buf()
    local offset_encoding = vim.lsp.buf_get_clients(bufnr)[1].offset_encoding
    if item.additionalTextEdits then
      vim.lsp.util.apply_text_edits(item.additionalTextEdits, bufnr, offset_encoding)
    end
end

M.setup = function(lang_lsp_opts_tuple)
    menu_opts['Code action [A-Enter]'] = vim.lsp.buf.code_action
    menu_opts['Code complete done [(i) C-l]'] = M.complete_done
    menu_opts['Find references'] = vim.lsp.buf.references
    menu_opts['Format'] = vim.lsp.buf.format
    menu_opts['Go to declaration'] = vim.lsp.buf.declaration
    menu_opts['Go to definition [gd]'] = vim.lsp.buf.definition
    menu_opts['Go to implementation'] = vim.lsp.buf.implementation
    menu_opts['Go to type definition'] = vim.lsp.buf.type_definition
    menu_opts['Hover [K]'] = vim.lsp.buf.hover
    menu_opts['List errors'] = vim.diagnostic.setloclist
    menu_opts['Rename symbol'] = vim.lsp.buf.rename
    menu_opts['Show line diagnostics'] = vim.diagnostic.open_float
    menu_opts['Document highlight'] = function()
        vim.lsp.buf.clear_references()
        vim.lsp.buf.document_highlight()
    end
    menu_opts['Document symbols'] = vim.lsp.buf.document_symbol
    menu_opts['Workspace symbols'] = vim.lsp.buf.workspace_symbol
    menu_opts['Incoming calls'] = vim.lsp.buf.incoming_calls
    menu_opts['Outgoing calls'] = vim.lsp.buf.outgoing_calls
    menu_opts['Outgoing calls'] = vim.lsp.buf.outgoing_calls
    menu_opts['Signature help [C-K]'] = vim.lsp.buf.signature_help

    for key, _ in pairs(menu_opts) do
      table.insert(menu_keys, key)
    end

    for _, p in ipairs(lang_lsp_opts_tuple) do
        lang, lsp, opts = unpack(p)
        -- Use a loop to conveniently call 'setup' on multiple servers and
        -- map buffer local keybindings when the language server attaches
        setup_attrs = {
            on_attach = on_attach,
            flags = {
                debounce_text_changes = 150,
            }
        }

        for k, v in pairs(opts) do
              setup_attrs[k] = v
        end



        nvim_lsp[lsp].setup(setup_attrs)
        -- LSP menu
        local base_autocmd = 'autocmd FileType '.. lang .. ' '
        vim.cmd(base_autocmd .. 'nnoremap <buffer> <C-l> :lua require("lang/lsp").show_menu()<CR>')
        vim.cmd(base_autocmd .. 'vnoremap <buffer> <C-l> :lua require("lang/lsp").show_menu()<CR>')
        vim.api.nvim_command(base_autocmd .. [[autocmd CursorHold  <buffer> lua pcall(vim.lsp.buf.document_highlight)]])
        vim.api.nvim_command(base_autocmd .. [[autocmd CursorHoldI <buffer> lua pcall(vim.lsp.buf.document_highlight)]])
        vim.api.nvim_command(base_autocmd .. [[autocmd CursorMoved <buffer> lua pcall(vim.lsp.buf.clear_references)]])
    end
end

return M
