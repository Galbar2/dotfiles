--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--


-- " Read jdt files
vim.cmd [[
function! ShowJDTContentAsReadOnly(response)
    call setline(1, split(get(a:response, 'result', ''), '\n'))
    set buftype=
    setl readonly
    setl nomodified
endfunction
function! LoadJavaJDTContent(uri)
    setfiletype java
    call LanguageClient#Call("java/classFileContents", {'uri': a:uri}, function('ShowJDTContentAsReadOnly'))
endfunction
au BufReadCmd,FileReadCmd,SourceCmd jdt://* call LoadJavaJDTContent(expand("<amatch>"))
]]
