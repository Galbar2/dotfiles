--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--
util = require "lspconfig/util"

local gopls_settings = {
    cmd = {"gopls", "serve"},
    filetypes = {"go", "gomod", "gotmpl", "gowork"},
    root_dir = util.root_pattern("go.work", "go.mod", ".git"),
    settings = {
        gopls = {
            analyses = {
                unusedparams = true,
            },
            staticcheck = true,
        },
    },
}

require("mason").setup()
require("mason-lspconfig").setup({
    ensure_installed = {
        "pylsp",
        "ts_ls",
    },
})

--  pip install python-lsp-black pylsp-rope pylsp-mypy python-lsp-server 'python-lsp-server[rope]'
require('lang/lsp').setup({
    {'go', 'gopls', gopls_settings},
    {'gomod', 'gopls', gopls_settings},
    {'gotmpl', 'gopls', gopls_settings},
    {'gowork', 'gopls', gopls_settings},
    {'java', 'jdtls', { cmd = {'jdtls'}}},
    {'python', 'pylsp', {
        settings = {
            pylsp = {
                plugins = {
                    black = {enabled = true},
                    yark = {enabled = false},
                    pylint = {enabled = false},
                    autopep8 = {enabled = false},
                    yapf = {enabled = false},
                    flake8 = {enabled = true},
                    pycodestyle = {enabled = false},
                    mypy = {enabled = true},
                    rope_autoimport = {enabled = true},
                    isort = {enabled = true},
                },
                configurationSources = {"pycodestyle", "flake8"}
            }
        }
    }},
    {'ruby', 'sorbet', {}},
    {'ruby', 'rubocop', {}},
    {'php', 'phpactor', {}},
    {'javascript', 'ts_ls', {}},
    {'javascript.jsx', 'ts_ls', {}},
    {'javascriptreact', 'ts_ls', {}},
    {'terraform', 'terraformls', {}},
    {'typescript', 'ts_ls', {}},
    {'typescript.tsx', 'ts_ls', {}},
    {'typescriptreact', 'ts_ls', {}},
    {'vue', 'vuels', {}},
    {'solidity', 'solc', {}},
    {'sql', 'sqlls', {}},
})
require 'lang/javascript'
require 'lang/typescript'
require 'lang/vue'
require 'lang/java'
