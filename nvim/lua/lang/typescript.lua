--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--


-- Format
local tabsize = '2'
vim.cmd ( 'autocmd FileType typescript set expandtab' )
vim.cmd ( 'autocmd FileType typescript set shiftwidth=' .. tabsize )
vim.cmd ( 'autocmd FileType typescript set softtabstop=' .. tabsize )
vim.cmd ( 'autocmd FileType typescript set tabstop=' .. tabsize )
vim.cmd ( 'autocmd FileType typescriptreact set expandtab' )
vim.cmd ( 'autocmd FileType typescriptreact set shiftwidth=' .. tabsize )
vim.cmd ( 'autocmd FileType typescriptreact set softtabstop=' .. tabsize )
vim.cmd ( 'autocmd FileType typescriptreact set tabstop=' .. tabsize )

local M = {}

-- Format file on save
M.format_prettier = function()
    local pos = vim.fn.getpos '.'
    vim.cmd("silent %!prettier --no-error-on-unmatched-pattern --parser typescript --stdin-filepath '" .. vim.fn.expand("%:p") .. "'")
    vim.cmd [[
    if v:shell_error != 0
        undo
    endif
    ]]
    vim.fn.setpos('.', pos)
end

M.format_eslint = function()
    local pos = vim.fn.getpos '.'
    vim.cmd("silent %!eslint --ignore-path .eslintignore --config .eslintrc.js '" .. vim.fn.expand("%:p") .. "'")
    vim.cmd [[
    if v:shell_error != 0
        undo
    endif
    ]]
    vim.fn.setpos('.', pos)
end

M.format_file = function()
    M.format_eslint()
end

vim.cmd 'autocmd FileType typescript autocmd BufWritePre <buffer> lua require("lang/typescript").format_file()'
vim.cmd 'autocmd FileType typescriptreact autocmd BufWritePre <buffer> lua require("lang/typescript").format_file()'

return M
