--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--


-- Format
vim.cmd 'autocmd FileType vue set noexpandtab'
vim.cmd 'autocmd FileType vue set shiftwidth=4'
vim.cmd 'autocmd FileType vue set softtabstop=4'
vim.cmd 'autocmd FileType vue set tabstop=4'

local M = {}

-- Format file on save
M.format_file = function()
    local pos = vim.fn.getpos '.'
    vim.cmd('silent %!prettier --no-error-on-unmatched-pattern --parser vue --stdin-filepath ' .. vim.fn.expand("%:p"))
    vim.cmd [[
    if v:shell_error != 0
        undo
    endif
    ]]
    vim.fn.setpos('.', pos)
end

vim.cmd 'autocmd FileType vue autocmd BufWritePre <buffer> lua require("lang/vue").format_file()'

return M
