--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

-- Generic config
vim.g.mapleader = ','
vim.opt.backspace = 'indent,eol,start'                -- Backspace for dummies
vim.opt.linespace = 0                                 -- No extra spaces between rows
vim.opt.nu = true                                     -- Line numbers on
vim.opt.showmatch = true                              -- Show matching brackets/parenthesis
vim.opt.incsearch = true                              -- Find as you type search
vim.opt.hlsearch = true                               -- Highlight search terms
vim.opt.winminheight = 0                              -- Windows can be 0 line high
vim.opt.ignorecase = true                             -- Case insensitive search
vim.opt.smartcase = true                              -- Case sensitive when uc present
vim.opt.wildmenu = true                               -- Show list instead of just completing
vim.opt.wildmode = 'list:longest,full'                -- Command <Tab> completion, list matches, then longest common part, then all.
vim.opt.whichwrap = 'b,s,h,l,<,>,[,]'                 -- Backspace and cursor keys wrap too
vim.opt.scrolljump = 5                                -- Lines to scroll when cursor leaves screen
vim.opt.scrolloff = 3                                 -- Minimum lines to keep above and below cursor
vim.opt.list = true                                   -- Show problematic spaces
vim.opt.backup = false                                -- no backup - use git like a normal person
vim.opt.swapfile = false                              -- no swap file
vim.opt.splitbelow = true                             -- horizontal windows always split below
vim.opt.splitright = true                             -- vertical windows always split right
vim.opt.completeopt = 'menu'
vim.opt.title = true                                  -- show window title
vim.opt.autoindent = true                             -- autoindent when pressing Enter
vim.opt.background = 'dark'
vim.opt.tabstop = 4                                   -- use 4 spaces for tabs
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = true
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.ls = 2
vim.opt.ruler = true
vim.opt.showtabline = 3
vim.opt.formatoptions = 'qroct'
vim.opt.showcmd = true
vim.opt.clipboard = 'unnamed'                         -- save to system's clipboard
vim.opt.mouse = 'a'                                   -- allow mouse usage for all modes (a)
vim.opt.spelllang = 'en_us'                           -- current language
vim.opt.cursorline = true                             -- highlight the current line
vim.opt.fileformat = 'unix'                           -- unix file format by default
vim.opt.fileformats = 'unix,dos,mac'                  -- available formats
vim.opt.listchars = 'tab:› ,trail:•,extends:#,nbsp:.' -- Highlight problematic whitespace
vim.opt.colorcolumn = '120'                             -- set ruler at 120
vim.opt.laststatus = 2                                -- Always display the statusline in all windows
vim.opt.showmode = false                              -- Hide the default mode text (e.g. -- INSERT -- below the statusline)
vim.opt.wrap = false
vim.opt.vb = true                                     -- Kill sounds
vim.opt.relativenumber = true

-- turn syntax dection on
vim.cmd 'syntax enable'
