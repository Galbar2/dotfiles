--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

-- Remove whitespace on close
vim.cmd 'autocmd FileType c,cpp,java,go,php,javascript,python,twig,xml,yml,perl,lua autocmd BufWritePre <buffer> call StripTrailingWhitespace()'
--
-- StriTrailingWhitespace - taken from http://spf13.com
-- flag to manually disable trailing white space cleanning
vim.cmd [[
let g:cleanSpacesOnSave = 1
function! StripTrailingWhitespace()
    " Don't do if disabled.
    if g:cleanSpacesOnSave == 0
        return
    endif
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " do the business:
    %s/\s\+$//e
    " clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction
]]
