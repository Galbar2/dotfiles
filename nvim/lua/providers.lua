--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

-- Python provider (:help python-provider)

-- Python
local py3_lines = vim.split(vim.fn.execute '!pyenv prefix nvim-py3', '\n')
local py3_prefix = py3_lines[#py3_lines - 1]
vim.g.python3_host_prog = py3_prefix .. '/bin/python'

-- Ruby
vim.g.ruby_host_prog = vim.fn.stdpath('config') .. '/bin/neovim-ruby-host'
