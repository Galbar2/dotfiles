--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--


-- This module requires fzf nvim plugin to be installed

local M = {}


-- Open a menu at the cursor position with the options in the `source` (array
-- of strings) and a callback in the `sink`. The callback will receive the
-- selected option (string).
-- `max_opts` sets the maximum number of options displayed by the menu
-- `width` is the width of the menu
-- `prompt` is optional
M.show = function(source, sink, max_opts, width, prompt)
    local win_height = vim.go.lines
    local win_width = vim.go.columns
    local cury, curx = unpack(vim.api.nvim_win_get_cursor(0))
    local screen_pos = vim.call('screenpos', 0, cury, curx)

    local menu_height = (#source > max_opts and max_opts or #source) + 4
    local menu_width = width
    local options = {}
    table.insert(options,  '--info=hidden' )

    if screen_pos.row >= (win_height - menu_height) then
        screen_pos.row = screen_pos.row - menu_height - 1
        table.insert(options, '--layout=default')
    else
        table.insert(options, '--layout=reverse')
    end

    if prompt then
        table.insert(options, '--prompt=' .. prompt)
    end

    local window ={}
    window.width = menu_width
    window.height = menu_height
    -- window.relative = false
    window.xoffset = (screen_pos.col) / (win_width - menu_width)
    window.yoffset = (screen_pos.row + 1) / (win_height - menu_height)

    local args = {}
    args.source = source
    args.sink = sink
    args.options = options
    args.window = window


    vim.call('fzf#run', args)
end


return M
