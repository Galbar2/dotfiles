--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--


local ctx_menu = require('contextual-menu')

local M = {}

M.setup = function()
    local default_ui_selector = vim.ui.select

    vim.ui.select = function(items, opts, on_choice)
        local formatted_items = {}
        local reverse_idx = {}
        for k, v in pairs(items) do
            local formatted_value = tostring(v)
            if opts.format_item then
                formatted_value = opts.format_item(v)
            end
            formatted_items[k] = formatted_value
            reverse_idx[formatted_value] = {v, k}
        end

        local custom_choice = function(selected_item)
            on_choice(unpack(reverse_idx[selected_item]))
        end
        ctx_menu.show(formatted_items, custom_choice, 20, 100, opts.prompt)
    end
end

return M

