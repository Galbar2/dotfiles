--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

vim.cmd 'let test#strategy = "neovim"'
vim.cmd 'let test#python#runner = "pytest"'

vim.cmd 'let test#javascript#runner = "reactscripts"'
vim.cmd 'let g:test#javascript#reactscripts#executable = "npm run test -- --watchAll=false --bail"'
