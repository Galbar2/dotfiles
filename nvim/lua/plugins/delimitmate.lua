--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL

vim.delimitMate_expand_space = 1
vim.delimitMate_jump_expansion = 1
vim.delimitMate_expand_cr = 1
