function nvim_inside_nvim(port, _)
  require("remote-nvim.ui").float_term(("nvim --server localhost:%s --remote-ui"):format(port), function(exit_code)
    if exit_code ~= 0 then
      vim.notify(("Local client failed with exit code %s"):format(exit_code), vim.log.levels.ERROR)
    end
  end)
end

function nvim_inside_tmux_window(port, workspace_config)
  local cmd = ("tmux new-window -n %s nvim --server localhost:%s --remote-ui"):format(
    ("'Remote: %s'"):format(workspace_config.host),
    port
  )
  vim.fn.jobstart(cmd, {
    detach = true,
    on_exit = function(job_id, exit_code, event_type)
      -- This function will be called when the job exits
      print("Client", job_id, "exited with code", exit_code, "Event type:", event_type)
    end,
  })
end


require("remote-nvim").setup({
  remote = {
    -- List of directories that should be copied over
    copy_dirs = {
      -- What to copy to remote's Neovim config directory
      config = {
        base = vim.fn.stdpath("config"), -- Path from where data has to be copied
        dirs = "*", -- Directories that should be copied over. "*" means all directories. To specify a subset, use a list like {"lazy", "mason"} where "lazy", "mason" are subdirectories
        -- under path specified in `base`.
        compression = {
          enabled = true, -- Should compression be enabled or not
          additional_opts = {} -- Any additional options that should be used for compression. Any argument that is passed to `tar` (for compression) can be passed here as separate elements.
        },
      },
    },
  },
  client_callback = function(port, workspace_config)
    local is_tmux = vim.env.TMUX ~= nil
    local is_foot = false

    if is_tmux then
        nvim_inside_tmux_window(port, workspace_config)
    elseif is_foot then
    --     ...
    else
        nvim_inside_nvim(port, workspace_config)
    end
  end,
})
