--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

-- NERDTree management
vim.api.nvim_set_keymap('', '<leader>b', ':NERDTreeToggle<CR>', {})
