--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

-- find word
vim.g.ackprg = 'ag --vimgrep --smart-case'
vim.g.ack_autoclose = 1
vim.g.ackhighlight = 1
vim.g.ack_use_cword_for_empty_search = 1
vim.cmd 'cnoreabbrev ag Ack!'
vim.cmd 'cnoreabbrev aG Ack!'
vim.cmd 'cnoreabbrev Ag Ack!'
vim.cmd 'cnoreabbrev AG Ack!'
