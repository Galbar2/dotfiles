--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

local plug_file = vim.env.HOME .. '/.config/nvim/autoload/plug.vim'
local new_install = false
if vim.loop.fs_stat(plug_file) == nil
then
    vim.cmd("!curl -fLo " .. plug_file .. " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim")
    vim.cmd("source " .. plug_file)
    new_install = true
end

vim.cmd [[
call plug#begin('~/.config/nvim/plugged')

" Common dependencies
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } " UI for LSP
Plug 'nvim-lua/plenary.nvim'


" IDE / Way of working
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
Plug 'nvim-tree/nvim-web-devicons'
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'ibhagwan/fzf-lua', {'branch': 'main'}

Plug 'nvim-tree/nvim-tree.lua'
Plug 'tpope/vim-sensible'
Plug 'airblade/vim-gitgutter'
Plug 'honza/vim-snippets'
Plug 'vim-test/vim-test'
Plug 'kien/rainbow_parentheses.vim'
Plug 'tomtom/tlib_vim'
Plug 'xolox/vim-misc'
Plug 'tyok/ack.vim'
Plug 'Lokaltog/vim-easymotion'
Plug 'Raimondi/delimitMate'
Plug 'terryma/vim-multiple-cursors'
Plug 'tommcdo/vim-exchange'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-commentary'
Plug 'michaeljsmith/vim-indent-object'

"  Utils
Plug 'godlygeek/tabular'
Plug 'tpope/vim-fugitive'
Plug 'xolox/vim-notes'
Plug 'diepm/vim-rest-console'
Plug 'tpope/vim-abolish'

" Themes
"Plug 'Galbar/promptline.vim'
"Plug 'edkolev/tmuxline.vim'
Plug 'srcery-colors/srcery-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"  Remote Development
Plug 'MunifTanjim/nui.nvim'
Plug 'amitds1997/remote-nvim.nvim'

Plug 'NStefan002/screenkey.nvim'

call plug#end()
]]

if new_install
then
    vim.cmd 'PlugInstall'
    vim.cmd 'PlugUpdate'
end

require('plugins/ack')
require('plugins/delimitmate')
require('plugins/fugitive')
require('plugins/nvimtree')
require('plugins/remote-nvim')
require('plugins/test')
require('plugins/vim-notes')
require('plugins/vim-rest-console')
