--
-- This is in the public domain, so feel free to use / change / redistribute it
-- You can check the complete repository at https://github.com/Galbar/dotfiles
--
-- Author: Alessio Linares <mail@alessio.cc>
-- License: LGPL
--

vim.cmd "let g:vrc_response_default_content_type = 'application/json'"
vim.cmd "let g:vrc_auto_format_response_patterns = {'json': 'jq'}"
vim.cmd "let g:vrc_auto_format_response_enabled = 1"
vim.cmd "let g:vrc_curl_opts = {'-s': '', '-i': ''}"
