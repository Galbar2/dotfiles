if [[ `uname` == 'Darwin' ]]
then
    eval "$(/opt/homebrew/bin/brew shellenv)"
    export PATH=/Applications/Docker.app/Contents/Resources/bin:$PATH
    plugins+=(tmux)
    ZSH_TMUX_AUTOSTART=true
    ZSH_TMUX_AUTOCONNECT=true
fi
