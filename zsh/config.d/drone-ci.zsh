local CONFIG_FILE=~/.config/drone-ci/config.json
if [[ -f "$CONFIG_FILE" ]]
then
    export DRONE_SERVER="$(cat $CONFIG_FILE | jq -r '.server')"
    export DRONE_TOKEN="$(cat $CONFIG_FILE | jq -r '.token')"
fi
