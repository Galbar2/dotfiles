# Project paths (pj) config
PROJECT_PATHS=()
for dir in ~/Programs ~/Work ~/src/github.com/;
do
    if [ -d "$dir" ];
    then
        for p in $(ls -d $dir/*/);
        do
            if [ ! -d "$p/.git" ];
            then
                num_all="$(ls -A "$p" | wc -l)"
                num_dirs="$( (ls -d -A $p*/) 2>/dev/null | wc -l)"
                if [[ "$num_all" -eq "$num_dirs" ]];
                then
                    PROJECT_PATHS=($PROJECT_PATHS "$p")
                fi
            fi
        done

        PROJECT_PATHS=($PROJECT_PATHS "$dir")
    fi
done
