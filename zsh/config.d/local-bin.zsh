local_bin="$HOME/.local/bin"

if [[ ! -d "$local_bin" ]]
then
    mkdir -p "$local_bin"
fi
# Exported by default
export PATH="${local_bin}:${PATH}"
