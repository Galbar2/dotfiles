export PATH="$HOME/.pyenv/bin:$PATH"
export PATH="$(pyenv root)/bin:$PATH"

eval "$(pyenv init --path)"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

eval "$(pip3 completion --zsh)"
eval "$(register-python-argcomplete vinfra)"

# Hack for pyright
export PYENV_VERSION=""
_py_unset_more_envs() {
    PYENV_VERSION=""
}
_py_set_more_envs() {
    PYENV_VERSION="${VIRTUAL_ENV##*/}"
}
typeset -g -a precmd_functions
if [[ -z $precmd_functions[(r)_py_unset_more_envs] ]]; then
  precmd_functions=(_py_unset_more_envs $precmd_functions);
fi
if [[ -z $precmd_functions[(r)_py_set_more_envs] ]]; then
  precmd_functions=($precmd_functions _py_set_more_envs);
fi
